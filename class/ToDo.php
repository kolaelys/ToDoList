<?php

/*
-id
-due_date
-priority
-done 
-text
-save()
-edit()
-delete()
-constructor 
----------------------------------------------------------------------------------------*/
class ToDo{

    private $done=false;
    private $text="Entrez la tâche à accomplir";
    private $id;

    public function __construct($text,$isDone = false){
        $this->setText($text);
        $this->setDone($isDone);
        $this->id=uniqid();
    }

   
    public function getText(){
        return $this->text;
    }

    public function getId(){
        return $this->id;
    }
    
    public function setText($value){
       $this->text=$value;
    }

    public function setDone($isDone){
         $this->done=$isDone;
    }
}

    function writeData($data, $filename='../db/todos.txt'){
        $tmp = serialize($data);
        file_put_contents($filename, $tmp); 
}


    function readData($filename='../db/todos.txt'){
        $data= file_get_contents($filename);
        $data = unserialize($data);
    
        return $data;
}

?>

